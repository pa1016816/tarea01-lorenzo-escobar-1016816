﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea01_lorenzo_escobar_1016816
{
    class Program
    {
        static void Main(string[] args)
        {
            //variables
            string respuesta;

            //objetos
            TarjetaDeCredito Tarjeta1 = new TarjetaDeCredito();

            while (true)
            {
                Console.Clear();
                try
                {
                    Console.WriteLine("************Validar números de tarjetas de crédito********");
                    Console.WriteLine("¿Cuántos números va a ingresar?");
                    respuesta = Console.ReadLine();
                    Tarjeta1.DefinirTamañoArreglo(int.Parse(respuesta)); //Defino el tamaño de mi arreglo

                    Console.WriteLine("Pasos para el código de Luhn");
                    Console.WriteLine("Ingrese los números:");
                    respuesta = Console.ReadLine();
                    Console.WriteLine("Invertir el orden de lo ingresado:");
                    Console.WriteLine(Tarjeta1.Invertir_LlenarArreglo(respuesta));
                    Console.WriteLine("Duplicar los números en posiciones impares:");
                    Console.WriteLine(Tarjeta1.DuplicarImpares());
                    Console.WriteLine("Sumar los dígitos de los que son mayores a 10:");
                    Console.WriteLine(Tarjeta1.SumarDigitosMayores10());
                    Console.WriteLine("\n Al sumar los números resultantes obtenemos que sus numeros son: ");
                    Console.WriteLine(Tarjeta1.validacion());



                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                Console.ReadKey();
            }
        }
    }
}
