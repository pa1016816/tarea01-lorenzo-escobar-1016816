﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tarea01_lorenzo_escobar_1016816
{
    class TarjetaDeCredito
    {
        int[] NumerosTarjeta;

        public void DefinirTamañoArreglo(int tamaño)
        {
            NumerosTarjeta = new int[tamaño];
        }

        public string Invertir_LlenarArreglo(string numero)
        {
            int contador = NumerosTarjeta.Length;
            string NumerosInvertidos="";

            for (int i=0; i<NumerosTarjeta.Length; i++)
            {
                contador -= 1;
                NumerosTarjeta[i] = int.Parse(numero[contador].ToString());
                NumerosInvertidos +=NumerosTarjeta[i]+" ";
            }

            return NumerosInvertidos;
        }
        
        
        public string DuplicarImpares()
        {
            string ImparesDuplicados = "";

            for (int i = 0; i < NumerosTarjeta.Length; i++)
            {
                if (i % 2 != 0)
                {
                    NumerosTarjeta[i] = NumerosTarjeta[i] * 2;
                }

                ImparesDuplicados += NumerosTarjeta[i] + " ";
            }

            return ImparesDuplicados;
        }

        public String SumarDigitosMayores10()
        {
            string mayores10 = "";

            for (int i=0; i<NumerosTarjeta.Length; i++)
            {
                switch (NumerosTarjeta[i].ToString())
                {
                    case "10":
                        NumerosTarjeta[i] = 1;
                        break;
                    case "12":
                        NumerosTarjeta[i] = 3;
                        break;
                    case "14":
                        NumerosTarjeta[i] = 5;
                        break;
                    case "16":
                        NumerosTarjeta[i] = 7;
                        break;
                    case "18":
                        NumerosTarjeta[i] = 9;
                        break;
                }
                mayores10 += NumerosTarjeta[i] + " ";
            }

            return mayores10;
        }

        public bool validacion()
        {
            int suma = 0;
            for (int i = 0; i < NumerosTarjeta.Length; i++)
            {
                suma += NumerosTarjeta[i];
            }

            if (suma % 10 == 0)
            {
                return true;
            }

            return false;
        }
    }
}
